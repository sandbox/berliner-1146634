<?php

/**
 * Main batch processing function for mass mail sending.
 */
function mailer_batch_mass_mail_process($mails, $values, &$context) {
  
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['max'] = count($mails);
  }

  $limit = 10;

  // With each pass through the callback, retrieve the next group of nids.
  $current_mails = array_slice($mails, $context['sandbox']['current'], $limit);
  foreach ($current_mails as $mail) {
    $result = mailer_batch_send_mass_mail($mail, $values);
    // Store some result for post-processing in the finished callback.
    $context['results'][] = t('Send to %mail: !result', array(
      '%mail' => $mail,
      '!result' => $result !== FALSE ? 'Success' : 'Failure',
    ));
    
    // Update our progress information.
    $context['sandbox']['progress']++;
    $context['sandbox']['current']++;
    $context['message'] = t('Now processing %mail', array('%mail' => $mail));
  }
  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
  
}

/**
 * Finished callback for mass mail sending batch process.
 */
function mailer_batch_mass_mail_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = count($results) .' processed.';
    $message .= theme('item_list', array('items' => $results));
    drupal_set_message($message);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
    drupal_set_message($message, 'error');
  }
}

/**
 * Send mail function for the mass mailer batch process.
 */
function mailer_batch_send_mass_mail($mail, $values) {
  $language = mailer_get_mail_language($mail);
  // Build dummy mailer object
  $mailer = (object) array(
    'delta' => 'mailer_mass_mail',
    'language' => $language,
    'subject' => $values['subject'],
    'body' => $values['body']['value'],
    'header' => array_filter($values['headers']),
    'html' => $values['body']['format'] != 'plain_text',
    'bcc'=> FALSE,
  );
  $params = array();
  $account = user_load_by_mail($mail);
  $variables = array();
  $token_options = array(
    'language' => $mailer->language,
    'sanitize' => FALSE,
    'clear' => TRUE,
  );
  if ($account) {
    $variables['user'] = $account;
    $token_options['callback'] = 'user_mail_tokens';
  }
  $mailer->subject = token_replace($mailer->subject, $variables, $token_options);
  $mailer->body = token_replace($mailer->body, $variables, $token_options);
  return _mailer_send($mailer, $mail);
}