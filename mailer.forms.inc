<?php

/**
 * Form builder for the form UI.
 */
function mailer_form($form, $form_state, $langcode = NULL) {

  if ($langcode === NULL) {
    $langcode = language_default()->language;
  }

  // get available mailers
  $mailer_types = mailer_get_mailer_types($langcode);

  if (!count($mailer_types)) {
    $form['message'] = array(
      '#type' => 'markup',
      '#markup' => t('Currently no modules provide custom mails using this interface. You can probably just uninstall this module (Mailer API) on the <a href="!url">module configuration page</a>.', array(
        '!url' => url('admin/modules', array(
          'fragment' => 'edit-modules-mail',
        )),
      )),
    );
    return $form;
  }

  $form['#tree'] = TRUE;

  // assemble the different modules
  $modules = array();
  foreach ($mailer_types as $mailer) {
    if (!in_array($mailer->module, $modules)) {
      $modules[] = $mailer->module;
    }
  }

  foreach ($modules as $module) {
    $module_info = system_get_info('module', $module);
    $form['mailer_group_' . $module . '_title'] = array(
      '#type' => 'markup',
      '#markup' => t('<h2>Mails defined by %group</h2>', array(
        '%group' => $module_info['name'],
      )),
    );
    $form['mailer_group_' . $module] = array(
      '#type' => 'vertical_tabs',
    );
  }

  // render a fieldset with settings for each mailer
  foreach ($mailer_types as $mailer) {

    $defaults = isset($mailer->defaults) ? $mailer->defaults : NULL;
    $anchor = $mailer->module . '-' . $mailer->delta;

    $form['mailer'][$mailer->delta] = array(
      '#type' => 'fieldset',
      '#title' => $mailer->title,
      '#collapsible' => TRUE,
      '#collapsed' => !(arg(3) == $anchor),
      '#description' => $mailer->description,
      '#group' => 'mailer_group_' . $mailer->module,
      '#prefix' => '<a name="' . $anchor . '"></a>',
    );

    $form['mailer'][$mailer->delta]['mailer'] = array(
      '#type' => 'value',
      '#value' => $mailer,
    );

    $form['mailer'][$mailer->delta]['active'] = array(
      '#access' => $mailer->module != 'system',
      '#type' => 'checkbox',
      '#title' => t('Active'),
      '#description' => t('Whether or not this mail should actually be send when the implementing module triggers the mail sending.'),
      '#default_value' => mailer_get_variable($mailer, 'active', isset($defaults['active']) ? $defaults['active'] : TRUE),
    );

    $form['mailer'][$mailer->delta]['subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#default_value' => mailer_get_variable($mailer, 'subject', isset($defaults['subject']) ? $defaults['subject'] : NULL),
    );

    $form['mailer'][$mailer->delta]['body'] = array(
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#default_value' => mailer_get_variable($mailer, 'body', isset($defaults['body']) ? $defaults['body'] : NULL),
    );

    // Theme token tree.
    $form['mailer'][$mailer->delta]['token_tree'] = array(
      '#type' => 'markup',
      '#markup' => theme('token_tree', array(
        'token_types' => array('site', 'user'),
        'global_types' => TRUE,
        'click_insert' => TRUE,
        'show_restricted' => FALSE,
        'recursion_limit' => 3,
        'dialog' => TRUE,
      )),
    );

    if (isset($mailer->token) && count($mailer->token)) {
    
      $form['mailer'][$mailer->delta]['token'] = array(
        '#type' => 'fieldset',
        '#title' => t('Additional placeholders'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#description' => t('The following additional placeholders can be used in the subject or body of the mail'),
      );
    
      $placeholders = '';
      foreach ($mailer->token as $key => $token) {
        $placeholders .='<dt>!' . $key . '</dt><dd>' . $token . '</dd>';
      }
    
      $form['mailer'][$mailer->delta]['token']['placeholders'] = array('#markup' => '<dl>' . $placeholders . '</dl>');
    
    }

    $form['mailer'][$mailer->delta]['bcc'] = array(
      '#type' => 'textfield',
      '#title' => t('BCC'),
      '#default_value' => mailer_get_variable($mailer, 'bcc', isset($defaults['bcc']) ? $defaults['bcc'] : NULL),
    );

    $form['mailer'][$mailer->delta]['html'] = array(
      '#type' => 'checkbox',
      '#title' => t('Send HTML mails'),
      '#default_value' => mailer_get_variable($mailer, 'html', isset($defaults['html']) ? $defaults['html'] : TRUE),
    );

    if (isset($mailer->test)) {
      $form['mailer'][$mailer->delta]['test'] = array(
        '#type' => 'fieldset',
        '#title' => t('Test'),
        '#description' => t('Send yourself an e-mail to see what it looks like. The e-mail will have the placeholders replaced with dummy-data. Please note, that any unsaved changes in this mail will <strong>not be reflected</strong> in the test mail.'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#tree' => TRUE,
        '#prefix' => '<div id="' . $mailer->module . '-' . $mailer->delta . '-test-form">',
        '#limit_validation_errors' => array(),
      );
      $form['mailer'][$mailer->delta]['test']['mail'] = array(
        '#type' => 'textfield',
        '#title' => t('Send testmail to the following address'),
        '#default_value' => variable_get('site_mail', ''),
      );
      $form['mailer'][$mailer->delta]['test']['submit'] = array(
        '#type' => 'button',
        '#name' => $mailer->module . '-' . $mailer->delta . '-test-submit',
        '#value' => t('Send test mail for !module: !delta', array(
          '!module' => $mailer->module,
          '!delta' => $mailer->delta,
        )),
        '#mailer' => $mailer,
        '#id' => 'edit-mailer-' . $mailer->module . '-' . $mailer->delta . '-send-test-mail-submit',
        '#ajax' => array(
          'callback' => 'mailer_send_test_mail_js',
          'wrapper' => $mailer->module . '-' . $mailer->delta . '-test-form',
          'method' => 'replace',
          'effect' => 'fade',
        ),
        '#limit_validation_errors' => array(
          array('mailer', $mailer->delta),
        ),
      );
    }

  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;

}

/**
 * Ajax callback for sending of a testmail.
 *
 * @param array $form 
 * @param array $form_state 
 * @return array
 */
function mailer_send_test_mail_js($form, &$form_state) {
  $mailer = $form_state['clicked_button']['#mailer'];
  // Only trigger the mail sending when no errors have occured.
  if (!form_get_errors()) {
    $mail = $form_state['values']['mailer'][$mailer->delta]['test']['mail'];
    if (mailer_send_test_mail($mailer, $mail)) {
      drupal_set_message(t('A test mail has been send to %mail.', array('%mail' => $mail)));
    }
    else {
      drupal_set_message(t('A Problem occured while trying to send a test mail to %mail.', array('%mail' => $mail)), 'error');
    };
  }
  return array($form['mailer'][$mailer->delta]['test']);
}

/**
 * Implementation of hook_validate().
 *
 * Only here for validation of mail address for the testing mails.
 */
function mailer_form_validate($form, &$form_state) {
  if (isset($form_state['values']['op']) && $form_state['values']['op'] == $form['submit']['#value']) {
    // main submit button, nothing to validate
    return;
  }
  $mailer = $form_state['clicked_button']['#mailer'];
  $mail = $form_state['values']['mailer'][$mailer->delta]['test']['mail'];
  if (empty($mail)) {
    form_set_error('mailer][' . $mailer->delta . '][test][mail', 'Please enter an email address');
  }
  elseif (!valid_email_address($mail)) {
    form_set_error('mailer][' . $mailer->delta . '][test][mail', 'This does not seem to be a valid email address');
  }
}

/**
 * Implementation of hook_submit().
 */
function mailer_form_submit($form, &$form_state) {
  if ($form_state['values']['op'] != $form['submit']['#value']) {
    return;
  }
  foreach ($form_state['values']['mailer'] as $setting) {
    $mailer = $setting['mailer'];
    mailer_set_variable($mailer, 'active', $setting['active']);
    mailer_set_variable($mailer, 'subject', $setting['subject']);
    mailer_set_variable($mailer, 'body', $setting['body']);
    mailer_set_variable($mailer, 'bcc', $setting['bcc']);
    mailer_set_variable($mailer, 'html', $setting['html']);
  }
  drupal_set_message(t('The settings have been saved.'));

}

/**
 * Form builder for the mass mail feature.
 */
function mailer_mass_mail_form($form, $form_state) {
  $form['mails'] = array(
    '#type' => 'textarea',
    '#title' => t('Mails'),
    '#description' => t('Enter the mail recipients, one email adress per line.'),
    '#default_value' => '',
    '#required' => TRUE,
  );
  $form['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('The mails subject text.'),
    '#default_value' => '',
    '#required' => TRUE,
  );
  $form['body'] = array(
    '#type' => 'text_format',
    '#title' => t('Body'),
    '#description' => t('The mails body text.'),
    '#default_value' => '',
    '#required' => TRUE,
  );
  $form['placeholders'] = array(
    '#type' => 'fieldset',
    '#title' => t('Placeholders'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('The following placeholders can be used in the subject or body of the mail'),
  );
  $form['placeholders']['tokens'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('user'),
    '#global_types' => TRUE,
    '#click_insert' => TRUE,
  );
  $form['headers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Headers'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Additional mail headers.'),
    '#tree' => TRUE,
  );
  $form['headers']['From'] = array(
    '#type' => 'textfield',
    '#title' => t('From'),
    '#description' => t('If not specified, the site mail will be used.'),
  );
  $form['headers']['Bcc'] = array(
    '#type' => 'textfield',
    '#title' => t('Bcc'),
    '#description' => t('You can specify one or more additional recipients. Specified mails will receive a copy of every single mail sended.'),
  );
  $form['actions']['send'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );
  return $form;
}

/**
 * Validation handler for the mass sending form.
 */
function mailer_mass_mail_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $mails = array_filter(explode("\n", $values['mails']));
  foreach ($mails as $key => $mail) {
    $mails[$key] = trim($mail);
  }

  $unvalidated_mails = array();
  foreach ($mails as $mail) {
    if (!valid_email_address($mail)) {
      $unvalidated_mails[] = $mail;
    }
  }
  if (count($unvalidated_mails) > 0) {
    form_error($form['mails'], t('The following mails seem incorrect:<br />!mails', array(
      '!mails' => implode("\n", $unvalidated_mails),
    )));
  }
  else {
    form_set_value($form['mails'], $mails, $form_state);
  }
}

/**
 * Submission handler for the mass sending form.
 */
function mailer_mass_mail_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $mails = $values['mails'];
  
  $batch = array(
    'operations' => array(
      array('mailer_batch_mass_mail_process', array($mails, $values)),
    ),
    'finished' => 'mailer_batch_mass_mail_finished',
    'title' => t('Processing mass mail sending'),
    'init_message' => t('Mass mail sending is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Mass mail sending has encountered an error.'),
    'file' => drupal_get_path('module', 'mailer') . '/mailer.batch.inc',
  );
  batch_set($batch);

  $form_state['rebuild'] = TRUE;
}