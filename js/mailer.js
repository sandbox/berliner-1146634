/**
 * Add some effects to the body field output of the mail log view
 */
Drupal.behaviors.MailerAdmin = function() {
  var row_height = '40px';
  var animation_duration = 400;
  $('.view-display-id-page_1 .view-content table td').css('height', row_height).css('vertical-align', 'top');
  $('.view-display-id-page_1 .view-content table td.views-field-body').each(function() {
    $(this).wrapInner('<div class="container">');
    var container = $(this).find('.container');
    $(container).css('height', row_height).css('overflow', 'hidden');
    $(container).hover(
      function() {
        $(this).stop();
        $(this)
          .data('oHeight', $(this).height())
          .css('height','auto')
          .data('nHeight', $(this).height())
          .height($(this).data('oHeight'))
          .animate({height: $(this).data('nHeight')}, animation_duration);
      },
      function() {
        $(this).stop();
        $(this).animate({height: row_height}, animation_duration);
      }
    );
  });
};