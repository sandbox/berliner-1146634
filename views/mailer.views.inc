<?php

/**
 * Views Hook Implementation.
 *
 * Defines some handlers for usage with views
 *
 */

/**
 * Defines some additional table relationships to work with views.
 *
 * @return array Views Table data structure
 */
function mailer_views_data() {
  $data = array();

  $data['mailer_log']['table']['base'] = array(
    'field' => 'id',
    'title' => 'Mailer Log',
    'help' => 'Mailer Log',
    'group' => 'Mailer Log',
  );

  $data['mailer_log']['table']['group'] = 'Mailer Log';
  
  // join with users table
  $data['users']['table']['join']['mailer_log'] = array(
    'field' => 'mail',
    'left_field' => 'mail',
  );
  
  // field declarations
  $data['mailer_log']['id'] = array(
    'title' => t('ID'),
    'help' => t('ID'),
    'field' => array('handler' => 'views_handler_field', 'click sortable' => TRUE),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
    'sort' => array('handler' => 'views_handler_sort'),
  );
  
  $data['mailer_log']['mail'] = array(
    'title' => t('Mail address'),
    'help' => t('Mail address'),
    'field' => array('handler' => 'views_handler_field', 'click sortable' => TRUE),
    'filter' => array('handler' => 'views_handler_filter_string'),
  );
  
  $data['mailer_log']['subject'] = array(
    'title' => t('Subject'),
    'help' => t('Mail subject'),
    'field' => array('handler' => 'views_handler_field', 'click sortable' => TRUE),
    'filter' => array('handler' => 'views_handler_filter_string'),
  );
  
  $data['mailer_log']['body'] = array(
    'title' => t('Body'),
    'help' => t('Mail body'),
    'field' => array('handler' => 'mailer_views_field_body', 'click sortable' => TRUE),
    'filter' => array('handler' => 'views_handler_filter_string'),
  );
  
  $data['mailer_log']['header'] = array(
    'title' => t('Header'),
    'help' => t('Mail headers'),
    'field' => array('handler' => 'mailer_views_field_header', 'click sortable' => TRUE),
  );
  
  $data['mailer_log']['status'] = array(
    'title' => t('Status'),
    'help' => t('Status of the mail'),
    'field' => array('handler' => 'views_handler_field', 'click sortable' => TRUE),
    'filter' => array('handler' => 'views_handler_filter_numeric'),
  );
  
  $data['mailer_log']['created'] = array(
    'title' => t('Date'),
    'help' => t('Date when the mail has been send'),
    'field' => array('handler' => 'mailer_views_field_created', 'click sortable' => TRUE),
    'sort' => array('handler' => 'views_handler_sort'),
  );
  
  return $data;
}
