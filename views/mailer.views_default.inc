<?php

/**
 * @file
 * Provides default views for the mailer module.
 */

/**
 * Implementation of hook_views_default_views().
 *
 * @return void
 */
function mailer_views_default_views() {

  $view = mailer_get_default_view_mail_log();
  $views[$view->name] = $view;
  
  return $views;

}

/**
 * Retrieve default view: mailer_log
 *
 * @return object
 */
function mailer_get_default_view_mail_log() {
  
  $view = new view();
  $view->name = 'mailer_log';
  $view->description = 'Mailer Log';
  $view->tag = '';
  $view->base_table = 'mailer_log';
  $view->human_name = '';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer mails';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = 50;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['grouping'] = '';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'mail' => 'mail',
    'subject' => 'subject',
    'body' => 'body',
    'status' => 'status',
    'header' => 'header',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'mail' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'subject' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'body' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'status' => array(
      'sortable' => 0,
      'separator' => '',
    ),
    'header' => array(
      'sortable' => 0,
      'separator' => '',
    ),
  );
  /* Field: Mailer Log: ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'mailer_log';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['label'] = '#';
  /* Field: Mailer Log: Date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'mailer_log';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Date';
  $handler->display->display_options['fields']['created']['decimal'] = '<p>.</p>';
  $handler->display->display_options['fields']['created']['separator'] = '<p>,</p>';
  /* Field: Mailer Log: Mail address */
  $handler->display->display_options['fields']['mail']['id'] = 'mail';
  $handler->display->display_options['fields']['mail']['table'] = 'mailer_log';
  $handler->display->display_options['fields']['mail']['field'] = 'mail';
  /* Field: Mailer Log: Subject */
  $handler->display->display_options['fields']['subject']['id'] = 'subject';
  $handler->display->display_options['fields']['subject']['table'] = 'mailer_log';
  $handler->display->display_options['fields']['subject']['field'] = 'subject';
  /* Field: Mailer Log: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'mailer_log';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = 'Mail body';
  /* Field: Mailer Log: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'mailer_log';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  /* Field: Mailer Log: Header */
  $handler->display->display_options['fields']['header']['id'] = 'header';
  $handler->display->display_options['fields']['header']['table'] = 'mailer_log';
  $handler->display->display_options['fields']['header']['field'] = 'header';
  $handler->display->display_options['fields']['header']['label'] = 'Mail header';
  /* Sort criterion: Mailer Log: Date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'mailer_log';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Mailer Log: Mail address */
  $handler->display->display_options['filters']['mail']['id'] = 'mail';
  $handler->display->display_options['filters']['mail']['table'] = 'mailer_log';
  $handler->display->display_options['filters']['mail']['field'] = 'mail';
  $handler->display->display_options['filters']['mail']['operator'] = 'contains';
  $handler->display->display_options['filters']['mail']['group'] = '0';
  $handler->display->display_options['filters']['mail']['exposed'] = TRUE;
  $handler->display->display_options['filters']['mail']['expose']['operator_id'] = 'mail_op';
  $handler->display->display_options['filters']['mail']['expose']['label'] = 'Mail address';
  $handler->display->display_options['filters']['mail']['expose']['operator'] = 'mail_op';
  $handler->display->display_options['filters']['mail']['expose']['identifier'] = 'mail';
  /* Filter criterion: Mailer Log: Subject */
  $handler->display->display_options['filters']['subject']['id'] = 'subject';
  $handler->display->display_options['filters']['subject']['table'] = 'mailer_log';
  $handler->display->display_options['filters']['subject']['field'] = 'subject';
  $handler->display->display_options['filters']['subject']['operator'] = 'contains';
  $handler->display->display_options['filters']['subject']['group'] = '0';
  $handler->display->display_options['filters']['subject']['exposed'] = TRUE;
  $handler->display->display_options['filters']['subject']['expose']['operator_id'] = 'subject_op';
  $handler->display->display_options['filters']['subject']['expose']['label'] = 'Mailer subject';
  $handler->display->display_options['filters']['subject']['expose']['operator'] = 'subject_op';
  $handler->display->display_options['filters']['subject']['expose']['identifier'] = 'subject';
  /* Filter criterion: Mailer Log: Body */
  $handler->display->display_options['filters']['body']['id'] = 'body';
  $handler->display->display_options['filters']['body']['table'] = 'mailer_log';
  $handler->display->display_options['filters']['body']['field'] = 'body';
  $handler->display->display_options['filters']['body']['operator'] = 'contains';
  $handler->display->display_options['filters']['body']['group'] = '0';
  $handler->display->display_options['filters']['body']['exposed'] = TRUE;
  $handler->display->display_options['filters']['body']['expose']['operator_id'] = 'body_op';
  $handler->display->display_options['filters']['body']['expose']['label'] = 'Mail body';
  $handler->display->display_options['filters']['body']['expose']['operator'] = 'body_op';
  $handler->display->display_options['filters']['body']['expose']['identifier'] = 'body';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Mail log';
  $handler->display->display_options['path'] = 'admin/reports/mail-log';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Mail Log';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['mailer_log'] = array(
    t('Standards'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('#'),
    t('Date'),
    t('<p>.</p>'),
    t('<p>,</p>'),
    t('Mail address'),
    t('Subject'),
    t('Mail body'),
    t('Status'),
    t('Mail header'),
    t('Mailer subject'),
    t('Page'),
    t('Mail log'),
  );
  return $view;
  
}