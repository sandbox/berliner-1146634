<?php

/**
 * Fieldhandler for mailer header column.
 * 
 */
class mailer_views_field_header extends views_handler_field {
  /**
   * Constructor.
   * 
   * @see sites/all/modules/views/handlers/views_handler_field#construct()
   */
  function construct() {
    parent::construct();
  }

  /**
   * Renders the status value as human readable strings.
   * 
   * @see sites/all/modules/views/handlers/views_handler_field#render($values)
   */
  function render($values) {
    $header = unserialize($values->{$this->field_alias});
    $items = array();
    if (is_array($header) && count($header)) {
      foreach ($header as $key => $value) {
        if (is_string($value)) {
          $items[] = $key . ': ' . $value;
        }
      }
    }
    return truncate_utf8(implode('<br />', $items), 80, FALSE, TRUE);
  }
}