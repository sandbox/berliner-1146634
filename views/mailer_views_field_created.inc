<?php

/**
 * Fieldhandler for mailer created column.
 * 
 */
class mailer_views_field_created extends views_handler_field_numeric {
  /**
   * Constructor.
   * 
   * @see sites/all/modules/views/handlers/views_handler_field#construct()
   */
  function construct() {
    parent::construct();
  }
  
  /**
   * Renders the value.
   * 
   * @see sites/all/modules/views/handlers/views_handler_field#render($values)
   */
  function render($values) {
    $val = $values->{$this->table . '_created'};
    return date('d.m.Y H:i:s', $val);
  }
}