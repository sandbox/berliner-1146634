<?php

/**
 * Fieldhandler for the mailer body columns.
 * 
 */
class mailer_views_field_body extends views_handler_field_markup {
  /**
   * Constructor.
   * 
   * @see sites/all/modules/views/handlers/views_handler_field#construct()
   */
  function construct() {
    drupal_add_js(drupal_get_path('module', 'mailer') . '/js/mailer.js');
    $this->definition['format'] = 'filtered_html';
    parent::construct();
  }

}