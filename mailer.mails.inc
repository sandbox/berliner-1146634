<?php

/**
 * Implements hook_mailer_info().
 *
 * Define some system mails.
 */
function mailer_mailer_info() {
  $mails = array();
  $mails['register_admin_created'] = array(
    'title' => t('An admin has created an account.'),
    'description' => NULL,
    'defaults' => array(
      'subject' => variable_get('user_mail_register_admin_created_subject', NULL),
      'body' => variable_get('user_mail_register_admin_created_body', NULL),
    ),
    'module' => 'system',
  );
  $mails['register_pending_approval'] = array(
    'title' => t('Registration received, awaiting approval.'),
    'description' => NULL,
    'defaults' => array(
      'subject' => variable_get('user_mail_register_pending_approval_subject', NULL),
      'body' => variable_get('user_mail_register_pending_approval_body', NULL),
    ),
    'module' => 'system',
  );
  $mails['register_no_approval_required'] = array(
    'title' => t('Registration received, account has been activated.'),
    'description' => NULL,
    'defaults' => array(
      'subject' => variable_get('user_mail_register_no_approval_required_subject', NULL),
      'body' => variable_get('user_mail_register_no_approval_required_body', NULL),
    ),
    'module' => 'system',
  );
  $mails['status_activated'] = array(
    'title' => t('User account has been activated.'),
    'description' => NULL,
    'defaults' => array(
      'subject' => variable_get('user_mail_status_activated_subject', NULL),
      'body' => variable_get('user_mail_status_activated_body', NULL),
    ),
    'module' => 'system',
  );
  $mails['status_blocked'] = array(
    'title' => t('User account has been blocked.'),
    'description' => NULL,
    'defaults' => array(
      'subject' => variable_get('user_mail_status_blocked_subject', NULL),
      'body' => variable_get('user_mail_status_blocked_body', NULL),
    ),
    'module' => 'system',
  );
  $mails['password_reset'] = array(
    'title' => t('A password reset has been requested.'),
    'description' => NULL,
    'defaults' => array(
      'subject' => variable_get('user_mail_password_reset_subject', NULL),
      'body' => variable_get('user_mail_password_reset_body', NULL),
    ),
    'module' => 'system',
  );
  $mails['cancel_confirm'] = array(
    'title' => t('An account deletion has been requested.'),
    'description' => NULL,
    'defaults' => array(
      'subject' => variable_get('user_mail_cancel_confirm_subject', NULL),
      'body' => variable_get('user_mail_cancel_confirm_body', NULL),
    ),
    'module' => 'system',
  );
  $mails['status_canceled'] = array(
    'title' => t('User account has been canceled.'),
    'description' => NULL,
    'defaults' => array(
      'subject' => variable_get('user_mail_status_canceled_subject', NULL),
      'body' => variable_get('user_mail_status_canceled_body', NULL),
    ),
    'module' => 'system',
  );
  $mails['contact_page_mail'] = array(
    'title' => t('Site wide contact form submission.'),
    'description' => NULL,
    'defaults' => array(
      'subject' => '[!category] !subject',
      'body' => "!sender-name (!sender-url) sent a message using the contact form at !form-url.

Message:
!message",
    ),
    'token' => array(
      'subject' => t('The subject of the contact mail.'),
      'sender-name' => t('The name of the sender.'),
      'sender-url' => t('The mail address of the sender.'),
      'message' => t('The message to be send.'),
    ),
    'module' => 'system',
  );
  $mails['contact_page_copy'] = $mails['contact_page_mail'];
  $mails['contact_page_copy']['title'] = t('Site wide contact form submission (copy).');

  $mails['contact_user_mail'] = array(
    'title' => t('Users contact form submission.'),
    'description' => NULL,
    'defaults' => array(
      'subject' => '[[site:name]] !subject',
      'body' => "Hello !recipient-name

!sender-name (!sender-url) has sent you a message via your contact form at [site:name].

If you don't want to receive such e-mails, you can change your settings at !recipient-edit-url.

Message:
!message",
    ),
    'token' => array(
      'subject' => t('The subject of the contact mail.'),
      'sender-name' => t('The name of the sender.'),
      'sender-url' => t('The mail address of the sender.'),
      'recipient-name' => t('The name of the recipient.'),
      'recipient-edit-url' => t('A url where the user can change the contact form settings.'),
      'message' => t('The message to be send.'),
    ),
    'module' => 'system',
  );
  $mails['contact_user_copy'] = $mails['contact_user_mail'];
  $mails['contact_user_copy']['title'] = t('Users contact form submission (copy).');
  
  return $mails;
}
