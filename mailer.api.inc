<?php

/**
 * Implements hook_mailer_info().
 *
 * Return an associative array of all mail types you will use in your module.
 *   key:    A unique name for the mail type (Used as $delta).
 *   value:  An associate array with the properties of the mail type.
 * For each different kind of mail that you want to register you need an
 * associative array with the following keys:
 *   (string)  title:
 *     The title of the mail type (used for the configuration screen)
 *   (string)  description:
 *     A description of the mail type (used for the configuration screen)
 *   (boolean) test:
 *     optional, default: FALSE
 *     Wheather this type is testable. If you set this to TRUE you must also
 *     implement hook_mailer_test().
 *   (array)   token:
 *     An associative array of available tokens for subject and body. The
 *     key is for internal use and the value is the human readable version
 *     that shows in the configuration screen
 *
 * @return array
 */
function hook_mailer_info() {
  
}

/**
 * Implements hook_mailer_values().
 *
 * @return void
 */
function hook_mailer_values($delta, $arguments = array()) {
  
}

/**
 * Implements hook_mailer_test().
 *
 * @param string $delta 
 * @param array $arguments 
 * @return void
 */
function hook_mailer_test($delta) {
  
}

/**
 * Implements hook_mailer_preprocess().
 *
 * Provides a possibility for other moduls to alter template variables.
 *
 * @param object $mailer 
 * @param array $variables 
 */
function hook_mailer_preprocess($mailer, &$variables) {
  
}

/**
 * Implements hook_mailer_mail_options_alter().
 *
 * @param array $mail_options 
 */
function hook_mailer_mail_options_alter(&$mail_options) {
  $mail_options['attachments'][] = array(
    'filecontent' => file_get_contents($file_path),
    'filename' => $file_name,
    'filemime' => 'appliation/pdf',
  );
}